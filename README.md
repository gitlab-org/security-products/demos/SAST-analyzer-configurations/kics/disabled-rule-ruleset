# kics-remote-ruleset

This project contains the SAST ruleset for the demo project [Project with Disabled Rule](https://gitlab.com/gitlab-org/security-products/demos/SAST-analyzer-configurations/kics/project-with-disabled-rule).

That project uses the `.gitlab/sast-ruleset.toml` in this project to configure the `kics` SAST-IaC scanner in that project.